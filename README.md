EN
# Client register

basic CRUD project for customer registration using spring boot and angular.

## Getting started

1 - run the project in the backend folder as a maven/spring-boot project

2 - run the project in the front-app folder as an angular project

3 - access the url http://localhost:4200/

        login: admin
        password: 123456

or

        login: user
        password: 123456

PT
# Client register

projeto CRUD básico para cadastro de clientes utilizando spring boot e angular.

## Começando

1 - execute o projeto na pasta backend como um projeto maven/spring-boot

2 - execute o projeto na pasta front-app como um projeto angular

3 - acesse a url http://localhost:4200/

         login: admin
         senha: 123456

ou

         login: user
         senha: 123456