package com.customer.register;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;

@EnableJdbcRepositories("com.customer.register.repository")
@SpringBootApplication
@Slf4j
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Autowired
    DataSource dataSource;

    @EventListener(ApplicationReadyEvent.class)
    public void loadData() {
        log.info("checking tables...");
        ResourceDatabasePopulator resourceDatabasePopulator =
                new ResourceDatabasePopulator(
                        false,
                        false,
                        "UTF-8",
                        new ClassPathResource("script.sql"));
        resourceDatabasePopulator.execute(dataSource);
        log.info("checking completed!");

    }
}
