package com.customer.register.controller;

import com.customer.register.model.Cellphone;
import com.customer.register.model.Client;
import com.customer.register.model.Email;
import com.customer.register.service.CellphoneService;
import com.customer.register.service.ClientService;
import com.customer.register.service.EmailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@CrossOrigin
@RestController
@RequestMapping("api/client")
public class ClientController {

    @Autowired
    ClientService service;

    @Autowired
    CellphoneService cellphoneService;

    @Autowired
    EmailService emailService;

    @PostMapping(value = "", consumes = {
            MediaType.APPLICATION_JSON_VALUE
    })
    @ResponseStatus(code = HttpStatus.CREATED)
    public ResponseEntity<Client> create(@RequestBody Client client) {
        return ResponseEntity.ok(service.create(client));
    }

    @PutMapping(value = "")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<Client> update(
            @RequestBody Client client) {
        client = service.update(client);
        return ResponseEntity.ok(client);
    }

    @GetMapping(value = "")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<List<Client>> listAll() {
        return ResponseEntity.ok(service.findAll());
    }

    @GetMapping(value = "/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<Client> findById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(service.find(id));
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public void delete(@PathVariable("id") Long id) {
        service.delete(id);
    }

    @DeleteMapping(value = "/cellphone/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public void deleteCellphone(@PathVariable("id") Long id) {
        cellphoneService.delete(id);
    }

    @DeleteMapping(value = "/email/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public void deleteEmail(@PathVariable("id") Long id) {
        emailService.delete(id);
    }

    @PostMapping(value = "/{id}/cellphone")
    @ResponseStatus(code = HttpStatus.CREATED)
    public void saveCellphone(@PathVariable("id") Long clientId, @RequestBody Cellphone cellphone) {
        cellphone.setClient(Client.builder().id(clientId).build());
        cellphoneService.create(cellphone);
    }

    @PostMapping(value = "/{id}/email")
    @ResponseStatus(code = HttpStatus.CREATED)
    public void saveEmail(@PathVariable("id") Long clientId, @RequestBody Email email) {
        email.setClient(Client.builder().id(clientId).build());
        emailService.create(email);
    }
}
