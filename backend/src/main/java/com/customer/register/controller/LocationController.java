package com.customer.register.controller;

import com.customer.register.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api/location")
public class LocationController {

    @Autowired
    LocationService service;

    @GetMapping(value = "/cep/{cep}")
    public @ResponseBody
    ResponseEntity<String> findCep(@PathVariable(value = "cep") String cep) {
        return new ResponseEntity<>(service.findCep(cep), HttpStatus.OK);
    }
}
