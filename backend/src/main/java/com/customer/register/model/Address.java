package com.customer.register.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Address implements Serializable {

    private Long id;

    private String zipCode;
    private String city;
    private String uf;
    private String district;
    private String address;
    private String complement;
}
