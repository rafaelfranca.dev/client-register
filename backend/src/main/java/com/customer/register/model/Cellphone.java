package com.customer.register.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Cellphone implements Serializable {

    private Long id;

    private String type;

    private String number;

    @JsonBackReference
    private Client client;
}
