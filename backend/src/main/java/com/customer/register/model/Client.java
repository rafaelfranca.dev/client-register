package com.customer.register.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Client implements Serializable {

    @Id
    private Long id;

    @Size(min = 3, message = "{validation.name.size.too_short}")
    @Size(max = 100, message = "{validation.name.size.too_long}")
    @Pattern(regexp = "^[a-zA-Z0-9]+$", message = "Nome inválido.")
    private String name;

    private String cpf;

    private Address address;

    @JsonManagedReference
    private List<Cellphone> phones;

    @JsonManagedReference
    private List<Email> emails;

    public Client(Long id, String name, String cpf, Address address) {
        this.id = id;
        this.name = name;
        this.cpf = cpf;
        this.address = address;
    }
}
