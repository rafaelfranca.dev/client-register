package com.customer.register.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Email implements Serializable {

    private Long id;

    private String email;

    @JsonBackReference
    private Client client;
}
