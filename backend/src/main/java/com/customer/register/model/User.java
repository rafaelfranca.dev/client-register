package com.customer.register.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@SuperBuilder
@AllArgsConstructor
@Builder
public class User implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 572419221797250437L;

    @JsonProperty(value = "username")
    private String username;

    @JsonIgnore
    private String password;

    @JsonProperty(value = "role")
    private String role;

}
