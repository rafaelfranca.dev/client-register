package com.customer.register.persistence;

import com.customer.register.model.Address;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressDAO extends GenericDao<Long, Address> {
}
