package com.customer.register.persistence;

import com.customer.register.model.Cellphone;
import com.customer.register.model.Client;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CellphoneDAO extends GenericDao<Long, Cellphone> {

    List<Cellphone> findByClient(Client id);
}
