package com.customer.register.persistence;

import com.customer.register.model.Client;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientDAO extends GenericDao<Long, Client> {
}
