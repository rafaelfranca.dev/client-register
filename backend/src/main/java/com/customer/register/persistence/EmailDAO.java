package com.customer.register.persistence;

import com.customer.register.model.Cellphone;
import com.customer.register.model.Client;
import com.customer.register.model.Email;

import java.util.List;

public interface EmailDAO extends GenericDao<Long, Email> {
    List<Email> findByClient(Client id);
}
