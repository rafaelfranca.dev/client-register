package com.customer.register.persistence;

import java.io.Serializable;
import java.util.List;

public interface GenericDao <ID, L extends Serializable> {

    L find(ID id);
    List<L> find();

    ID save(L instance);
    void update(L instance);
    void delete(ID instanceId);
}
