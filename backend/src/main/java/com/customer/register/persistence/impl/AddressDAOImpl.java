package com.customer.register.persistence.impl;

import com.customer.register.model.Address;
import com.customer.register.persistence.AddressDAO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;

@Repository
@Slf4j
public class AddressDAOImpl implements AddressDAO {

    @Autowired
    DataSource dataSource;


    @Override
    public Long save(Address address) {

        String sql = "INSERT INTO PUBLIC.ADDRESS(zip_code, city, uf, district, address, complement)VALUES(?, ?, ?, ?, ?,?)";
        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            int index = 1;
            ps.setString(index++, address.getZipCode());
            ps.setString(index++, address.getCity());
            ps.setString(index++, address.getUf());
            ps.setString(index++, address.getDistrict());
            ps.setString(index++, address.getAddress());
            ps.setString(index++, address.getComplement());
            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating client failed, no rows affected.");
            }

            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    address.setId(generatedKeys.getLong(1));
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                closeConnection(conn);
            }
        }
        return address.getId();
    }

    @Override
    public void update(Address address) {
        String sql = "UPDATE PUBLIC.ADDRESS SET zip_code=?, city=?, uf=?, district=?, address=?, complement=? WHERE id = ?";
        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            int index = 1;
            ps.setString(index++, address.getZipCode());
            ps.setString(index++, address.getCity());
            ps.setString(index++, address.getUf());
            ps.setString(index++, address.getDistrict());
            ps.setString(index++, address.getAddress());
            ps.setString(index++, address.getComplement());
            ps.setLong(index++, address.getId());
            log.debug("last index: {}", index);
            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("client update failed, no rows affected.");
            }

            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                closeConnection(conn);
            }
        }
    }

    @Override
    public Address find(Long id) {
        String sql = "SELECT * FROM ADDRESS WHERE id = ?";

        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setLong(1, id);
            Address address = null;
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                address = new Address(
                        rs.getLong("id"),
                        rs.getString("zip_code"),
                        rs.getString("city"),
                        rs.getString("uf"),
                        rs.getString("district"),
                        rs.getString("address"),
                        rs.getString("complement")
                );
            }
            rs.close();
            ps.close();
            return address;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                closeConnection(conn);
            }
        }
    }

    @Override
    public List<Address> find() {
        throw new NotImplementedException("DAO not implemented");
    }

    @Override
    public void delete(Long instanceId) {
        String sql = "DELETE FROM ADDRESS WHERE id = ?";

        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setLong(1, instanceId);

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("delete failed, no rows affected.");
            }

            ps.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                closeConnection(conn);
            }
        }
    }

    private void closeConnection(Connection conn) {
        try {
            conn.close();
        } catch (SQLException e) {
            log.error("Error on connection close: ", e);
        }
    }
}
