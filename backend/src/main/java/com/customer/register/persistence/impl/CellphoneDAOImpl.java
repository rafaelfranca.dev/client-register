package com.customer.register.persistence.impl;

import com.customer.register.model.Cellphone;
import com.customer.register.model.Client;
import com.customer.register.persistence.CellphoneDAO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
@Slf4j
public class CellphoneDAOImpl implements CellphoneDAO {

    @Autowired
    DataSource dataSource;

    @Override
    public Cellphone find(Long id) {
        String sql = "SELECT * FROM CELLPHONE WHERE id = ?";

        Connection conn = null;

        try {
            if (id == null) {
                throw new Exception("Id cannot be Null");
            }
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setLong(1, id);
            Cellphone cellphone = null;
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                cellphone = new Cellphone(
                        rs.getLong("id"),
                        rs.getString("type"),
                        rs.getString("number"),
                        Client.builder().id(rs.getLong("fk_client")).build()
                );
            }
            rs.close();
            ps.close();
            return cellphone;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                closeConnection(conn);
            }
        }
    }

    @Override
    public List<Cellphone> find() {
        throw new NotImplementedException("find not implemented");
    }

    @Override
    public Long save(Cellphone instance) {
        String sql = "INSERT INTO PUBLIC.CELLPHONE(TYPE, NUMBER,FK_CLIENT)VALUES(?, ?, ?)";
        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, instance.getType());
            ps.setString(2, instance.getNumber());
            ps.setLong(3, instance.getClient().getId());
            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating cellphone failed, no rows affected.");
            }

            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    instance.setId(generatedKeys.getLong(1));
                } else {
                    throw new SQLException("Creating cellphone failed, no ID obtained.");
                }
            }
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                closeConnection(conn);
            }
        }
        return instance.getId();
    }

    @Override
    public void update(Cellphone cellphone) {
        throw new NotImplementedException("Update not implemented");
    }

    @Override
    public void delete(Long instanceId) {
        String sql = "DELETE FROM CELLPHONE WHERE id = ?";

        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setLong(1, instanceId);

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("delete failed, no rows affected.");
            }

            ps.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                closeConnection(conn);
            }
        }
    }

    @Override
    public List<Cellphone> findByClient(Client client) {
        String sql = "SELECT * FROM CELLPHONE WHERE fk_client = ?";

        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setLong(1, client.getId());
            Cellphone cellphone = null;

            List<Cellphone> cellphoneList = new ArrayList<>();

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                cellphone = new Cellphone(
                        rs.getLong("id"),
                        rs.getString("type"),
                        rs.getString("number"),
                        client
                );
                cellphoneList.add(cellphone);
            }
            rs.close();
            ps.close();
            return cellphoneList;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                closeConnection(conn);
            }
        }
    }

    private void closeConnection(Connection conn) {
        try {
            conn.close();
        } catch (SQLException e) {
            log.error("Error on connection close: ", e);
        }
    }
}
