package com.customer.register.persistence.impl;

import com.customer.register.model.Address;
import com.customer.register.model.Client;
import com.customer.register.persistence.ClientDAO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
@Slf4j
public class ClientDAOImpl implements ClientDAO {

    @Autowired
    DataSource dataSource;


    @Override
    public Long save(Client client) {

        String sql = "INSERT INTO PUBLIC.CLIENT(NAME, CPF, FK_ADDRESS)VALUES(?, ?, ?)";
        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, client.getName());
            ps.setString(2, client.getCpf());
            ps.setLong(3, client.getAddress().getId());
            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating client failed, no rows affected.");
            }

            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    client.setId(generatedKeys.getLong(1));
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                closeConnection(conn);
            }
        }
        return client.getId();
    }

    @Override
    public void update(Client client) {
        String sql = "UPDATE PUBLIC.CLIENT SET NAME=?, CPF=?, FK_ADDRESS=? WHERE ID = ?";
        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, client.getName());
            ps.setString(2, client.getCpf());
            ps.setLong(3, client.getAddress().getId());
            ps.setLong(4, client.getId());
            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("client update failed, no rows affected.");
            }

            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                closeConnection(conn);
            }
        }
    }

    @Override
    public List<Client> find() {
        String sql = "SELECT * FROM client";

        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);

            Client client;

            List<Client> returnList = new ArrayList<>();

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                client = new Client(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("cpf"),
                        Address.builder().id(rs.getLong("fk_address")).build()
                );
                returnList.add(client);
            }
            rs.close();
            ps.close();
            return returnList;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                closeConnection(conn);
            }
        }
    }

    @Override
    public Client find(Long id) {
        String sql = "SELECT * FROM client WHERE id = ?";

        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setLong(1, id);
            Client customer = null;
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                customer = new Client(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("cpf"),
                        Address.builder().id(rs.getLong("fk_address")).build()
                );
            }
            rs.close();
            ps.close();
            return customer;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                closeConnection(conn);
            }
        }
    }

    @Override
    public void delete(Long instanceId) {
        String sql = "DELETE FROM CLIENT WHERE id = ?";

        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setLong(1, instanceId);

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("delete failed, no rows affected.");
            }

            ps.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                closeConnection(conn);
            }
        }
    }

    private void closeConnection(Connection conn) {
        try {
            conn.close();
        } catch (SQLException e) {
            log.error("Error on connection close: ", e);
        }
    }
}
