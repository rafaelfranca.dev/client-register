package com.customer.register.persistence.impl;

import com.customer.register.model.Client;
import com.customer.register.model.Email;
import com.customer.register.persistence.EmailDAO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
@Slf4j
public class EmailDAOImpl implements EmailDAO {

    @Autowired
    DataSource dataSource;

    @Override
    public Email find(Long aLong) {
        throw new NotImplementedException("Not Implemented");
    }

    @Override
    public List<Email> find() {
        throw new NotImplementedException("Not Implemented");
    }

    @Override
    public Long save(Email email) {
        String sql = "INSERT INTO EMAIL (EMAIL, FK_CLIENT)VALUES(?, ?)";
        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, email.getEmail());
            ps.setLong(2, email.getClient().getId());
            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating email failed, no rows affected.");
            }

            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    email.setId(generatedKeys.getLong(1));
                } else {
                    throw new SQLException("Creating email failed, no ID obtained.");
                }
            }
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                closeConnection(conn);
            }
        }
        return email.getId();
    }

    @Override
    public void update(Email instance) {
        throw new NotImplementedException("Not Implemented");
    }

    @Override
    public void delete(Long instanceId) {
        String sql = "DELETE FROM EMAIL WHERE id = ?";

        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setLong(1, instanceId);

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("delete failed, no rows affected.");
            }

            ps.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                closeConnection(conn);
            }
        }
    }

    @Override
    public List<Email> findByClient(Client client) {
        String sql = "SELECT * FROM EMAIL WHERE fk_client = ?";

        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setLong(1, client.getId());
            Email email;

            List<Email> emailList = new ArrayList<>();

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                email = new Email(
                        rs.getLong("id"),
                        rs.getString("email"),
                        client
                );
                emailList.add(email);
            }
            rs.close();
            ps.close();
            return emailList;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                closeConnection(conn);
            }
        }
    }

    private void closeConnection(Connection conn) {
        try {
            conn.close();
        } catch (SQLException e) {
            log.error("Error on connection close: ", e);
        }
    }
}
