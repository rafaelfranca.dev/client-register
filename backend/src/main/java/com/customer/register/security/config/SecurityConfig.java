package com.customer.register.security.config;

import com.customer.register.security.filter.JwtTokenFilter;
import com.customer.register.service.impl.UserServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Value("${security.basic.enabled:true}")
	private Boolean securityEnabled;

	private final JwtTokenFilter jwtTokenFilter;

	public static final String[] AUTH_WHITELIST = {
			"/api/auth/**",
			"/auth/**",
			"/",
			"/static/**",
			"/public/**",
	};

	private final UserServiceImpl userService;

	public SecurityConfig(JwtTokenFilter jwtTokenFilter, UserServiceImpl userService) {
		this.jwtTokenFilter = jwtTokenFilter;
		this.userService = userService;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(new UserServiceImpl());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		if (isSecurityEnabled()) {
			http = http.cors().and().csrf().disable();

			http = http
					.sessionManagement()
					.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
					.and();

			http = http
					.exceptionHandling()
					.authenticationEntryPoint(
							(request, response, ex) -> {
								response.sendError(
										HttpServletResponse.SC_UNAUTHORIZED,
										ex.getMessage()
								);
							}
					)
					.and();

			http.authorizeRequests()
					.antMatchers(AUTH_WHITELIST).permitAll()
					.anyRequest().authenticated();

			http.addFilterBefore(
					jwtTokenFilter,
					UsernamePasswordAuthenticationFilter.class
			);
		} else {
			http
				.csrf()
					.disable()
					.authorizeRequests()
					.anyRequest()
					.permitAll();
			log.warn("Security is not enabled!");
		}
	}

	@Bean
	public CorsFilter corsFilter() {
		UrlBasedCorsConfigurationSource source =
				new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOrigin("http://localhost:4200");
		config.addAllowedHeader("*");
		config.addAllowedMethod("*");
		source.registerCorsConfiguration("/**", config);
		return new CorsFilter(source);
	}

	@Bean(name = BeanIds.AUTHENTICATION_MANAGER)
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	public boolean isSecurityEnabled() {
		return securityEnabled;
	}
}