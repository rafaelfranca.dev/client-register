package com.customer.register.security.domain;

import com.customer.register.model.User;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
public class AuthenticationResponse implements Serializable {

    @JsonProperty(value = "access_token")
    private String token;

    @JsonProperty(value = "user")
    private User user;

    public AuthenticationResponse(String token) {
        super();
        this.token = token;
    }

}
