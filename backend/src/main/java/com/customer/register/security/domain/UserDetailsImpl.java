package com.customer.register.security.domain;

import com.customer.register.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;

public class UserDetailsImpl implements UserDetails {

	private String userName;
	private String password;
	private boolean isActive;
	private GrantedAuthority authorities;
	private User user;

	public UserDetailsImpl(User user) {
		this.userName = user.getUsername();
		this.password = user.getPassword();
		this.authorities = new SimpleGrantedAuthority(user.getRole());
		this.isActive = true;
		this.user = user;
	}

	public UserDetailsImpl() {
	}

	public User getUser() {
		return user;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Arrays.asList(authorities);
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return userName;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return isActive;
	}

	@Override
	public String toString() {
		return "UserDetailsImpl{" +
				"userName='" + userName + '\'' +
				", isActive=" + isActive +
				", authorities=" + authorities +
				", user=" + user +
				'}';
	}
}