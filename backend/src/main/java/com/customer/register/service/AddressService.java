package com.customer.register.service;

import com.customer.register.model.Address;
import com.customer.register.model.Client;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AddressService extends GenericService<Long, Address> {
}
