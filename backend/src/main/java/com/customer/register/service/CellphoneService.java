package com.customer.register.service;

import com.customer.register.model.Cellphone;
import com.customer.register.model.Client;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CellphoneService  extends GenericService<Long, Cellphone> {

    List<Cellphone> findByClient(Client client);

}
