package com.customer.register.service;

import com.customer.register.model.Client;
import org.springframework.stereotype.Service;

@Service
public interface ClientService extends GenericService<Long, Client> {
}
