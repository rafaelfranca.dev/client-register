package com.customer.register.service;

import com.customer.register.model.Cellphone;
import com.customer.register.model.Client;
import com.customer.register.model.Email;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface EmailService extends GenericService<Long, Email> {

    List<Email> findByClient(Client client);
}
