package com.customer.register.service;

import java.io.Serializable;
import java.util.List;

public interface GenericService<ID, E extends Serializable> {

    E create(E element);

    E update(E element);

    E find(ID id);

    List<E> findAll();

    void delete(ID id);
}
