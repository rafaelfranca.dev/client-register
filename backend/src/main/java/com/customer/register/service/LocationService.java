package com.customer.register.service;

import org.springframework.stereotype.Service;

@Service
public interface LocationService {
    String findCep(String cep) ;
}
