package com.customer.register.service;

import java.util.Map;

public interface LoginService {

    Map<String, String> login();
}
