package com.customer.register.service.impl;

import com.customer.register.model.Address;
import com.customer.register.persistence.AddressDAO;
import com.customer.register.service.AddressService;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    AddressDAO dao;

    @Override
    public Address create(Address address) {
        address.setId(dao.save(address));
        return address;
    }

    @Override
    public Address update(Address address) {
        dao.update(address);
        return address;
    }

    @Override
    public List<Address> findAll() {
        throw new NotImplementedException("Not Implemented");
    }

    @Override
    public Address find(Long id) {
        return dao.find(id);
    }

    @Override
    public void delete(Long id) {
        dao.delete(id);
    }

}
