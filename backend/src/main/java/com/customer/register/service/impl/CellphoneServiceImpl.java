package com.customer.register.service.impl;

import com.customer.register.model.Cellphone;
import com.customer.register.model.Client;
import com.customer.register.persistence.CellphoneDAO;
import com.customer.register.service.CellphoneService;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CellphoneServiceImpl implements CellphoneService {

    @Autowired
    CellphoneDAO dao;

    @Override
    public Cellphone create(Cellphone cellphone) {
        cellphone.setId(dao.save(cellphone));
        return cellphone;
    }

    @Override
    public Cellphone update(Cellphone element) {
        throw new NotImplementedException("Not Implemented");
    }

    @Override
    public Cellphone find(Long aLong) {
        throw new NotImplementedException("Not Implemented");
    }

    @Override
    public List<Cellphone> findAll() {
        throw new NotImplementedException("Not Implemented");
    }

    @Override
    public void delete(Long id) {
        dao.delete(id);
    }

    @Override
    public List<Cellphone> findByClient(Client client) {
        return dao.findByClient(client);
    }
}
