package com.customer.register.service.impl;

import com.customer.register.model.Address;
import com.customer.register.model.Cellphone;
import com.customer.register.model.Client;
import com.customer.register.model.Email;
import com.customer.register.persistence.impl.ClientDAOImpl;
import com.customer.register.service.AddressService;
import com.customer.register.service.CellphoneService;
import com.customer.register.service.ClientService;
import com.customer.register.service.EmailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ClientServiceImpl implements ClientService {

    @Autowired
    ClientDAOImpl dao;

    @Autowired
    AddressService addressService;

    @Autowired
    CellphoneService cellphoneService;

    @Autowired
    EmailService emailService;

    @Override
    public Client create(Client client) {

        Address address = addressService.create(client.getAddress());
        client.setAddress(address);

        Long id = dao.save(client);
        client.setId(id);

        client.setEmails(saveEmail(client.getEmails(), Client.builder().id(id).build()));

        client.setPhones(saveCellphone(client.getPhones(), Client.builder().id(id).build()));

        return client;
    }

    private List<Email> saveEmail(List<Email> emailList, Client client) {
        List<Email> savedEmails = new ArrayList<>();
        for (int i = 0; i < emailList.size(); i++) {
            Email email = emailList.get(i);
            email.setClient(client);
            email = emailService.create(email);
            savedEmails.add(email);
        }
        return savedEmails;
    }

    private List<Cellphone> saveCellphone(List<Cellphone> cellphoneList, Client client) {
        List<Cellphone> savedPhones = new ArrayList<>();
        for (int i = 0; i < cellphoneList.size(); i++) {
            Cellphone cellphone = cellphoneList.get(i);
            cellphone.setClient(client);
            cellphone = cellphoneService.create(cellphone);
            savedPhones.add(cellphone);
        }
        return savedPhones;
    }

    @Override
    public Client update(Client client) {
        dao.update(client);
        addressService.update(client.getAddress());
        return client;
    }

    @Override
    public Client find(Long id) {
        Client client = dao.find(id);
        if (client != null) {
            client.setAddress(addressService.find(client.getAddress().getId()));
            client.setPhones(cellphoneService.findByClient(client));
            client.setEmails(emailService.findByClient(client));
        }
        return client;
    }

    @Override
    public List<Client> findAll() {
        List<Client> data = dao.find();
        if (data != null && !data.isEmpty()) {
            for (int i = 0; i < data.size(); i++) {
                data.get(i).setAddress(addressService.find(data.get(i).getAddress().getId()));
                data.get(i).setPhones(cellphoneService.findByClient(data.get(i)));
                data.get(i).setEmails(emailService.findByClient(data.get(i)));
            }
        }
        return data;
    }

    @Override
    public void delete(Long id) {
        Client client = find(id);

        if (client.getAddress() != null && client.getAddress().getId() != null) {
            addressService.delete(client.getAddress().getId());
        }

        if (client.getPhones() != null) {
            for (Cellphone phone : client.getPhones()) {
                cellphoneService.delete(phone.getId());
            }
        }
        if (client.getEmails() != null) {
            for (Email email : client.getEmails()) {
                emailService.delete(email.getId());
            }
        }
        dao.delete(id);
    }

//    @Override
//    public void validate(@NotNull  Client client) {
//        if (client.getName().length() < 3 ) {
//
//        }
//    }
}
