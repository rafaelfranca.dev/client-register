package com.customer.register.service.impl;

import com.customer.register.model.Client;
import com.customer.register.model.Email;
import com.customer.register.persistence.EmailDAO;
import com.customer.register.service.EmailService;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    EmailDAO dao;

    @Override
    public Email create(Email element) {
        element.setId(dao.save(element));
        return element;
    }

    @Override
    public Email update(Email element) {
        throw new NotImplementedException("Not Implemented");
    }

    @Override
    public Email find(Long aLong) {
        throw new NotImplementedException("Not Implemented");
    }

    @Override
    public List<Email> findAll() {
        throw new NotImplementedException("Not Implemented");
    }

    @Override
    public void delete(Long id) {
        dao.delete(id);
    }

    @Override
    public List<Email> findByClient(Client client) {
        return dao.findByClient(client);
    }
}
