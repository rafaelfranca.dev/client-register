package com.customer.register.service.impl;

import com.customer.register.service.LocationService;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class LocationServiceImpl implements LocationService {


	@Override
	public String findCep(String cep) {

		RestTemplate restTemplate = new RestTemplate();

		String url = "https://viacep.com.br/ws/"+cep+"/json/";
		return restTemplate.getForObject(url, String.class);
	}
}
