package com.customer.register.service.impl;

import com.customer.register.model.User;
import com.customer.register.security.domain.UserDetailsImpl;
import com.customer.register.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

	@Override
	public UserDetailsImpl loadUserByUsername(String username) throws UsernameNotFoundException {

		UserDetailsImpl user;
//		Lógica simplificada.
		String PASSWORD = "123456";
		switch (username) {
			case "admin":
				user = new UserDetailsImpl(User.builder().username(username).password(PASSWORD).role("ADMIN").build());
				break;
			case "user":
				user = new UserDetailsImpl(User.builder().username(username).password(PASSWORD).role("USER").build());
				break;
			default:
				throw new UsernameNotFoundException("User not Found!");
		}

		return user;
	}

}