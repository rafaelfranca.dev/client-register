package com.customer.register.web.error;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Objects;

@Slf4j(topic = "GLOBAL_EXCEPTION_HANDLER")
@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	public static final String TRACE = "trace";

	private boolean printStackTrace;

	@Value("${reflectoring.trace:false}")
	public void setPrintStackTrace(boolean printStackTrace) {
		this.printStackTrace = printStackTrace;
	}

	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	public ResponseEntity<Object> handleIllegalArgumentException(Exception exception, WebRequest request) {
		String message = "Erro ao processar parâmetros";
		log.error(message, exception);
		return buildErrorResponse(exception, message, HttpStatus.UNPROCESSABLE_ENTITY, request);
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseEntity<Object> handleAllUncaughtException(Exception exception, WebRequest request) {
		String message = exception.getLocalizedMessage();
		log.error(message, exception);
		return buildErrorResponse(exception, message, HttpStatus.INTERNAL_SERVER_ERROR, request);
	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	@ResponseStatus(HttpStatus.CONFLICT)
	public ResponseEntity<Object> handleDataIntegrityViolationException(DataIntegrityViolationException exception, WebRequest request) {
		String message = "Conflito ao alterar entidade";
		String detail = exception.getMostSpecificCause().getMessage();
		log.error(message + detail, exception);
		return buildErrorResponse(exception, detail, HttpStatus.CONFLICT, request);
	}

	@ExceptionHandler(InvalidDataAccessApiUsageException.class)
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	public ResponseEntity<Object> handleInvalidDataAccessApiUsageException(InvalidDataAccessApiUsageException exception, WebRequest request) {
		String message = "Erro ao Processar dados: ";
		log.error(message, exception);
		return buildErrorResponse(exception, message, HttpStatus.NOT_FOUND, request);
	}

	@ExceptionHandler(BadCredentialsException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public ResponseEntity<Object> badCredentions(BadCredentialsException exception, WebRequest request) {
		String message = "Senha ou Login Inválido!";
		log.error(message, exception);
		return buildErrorResponse(exception, message, HttpStatus.UNAUTHORIZED, request);
	}

	private ResponseEntity<Object> buildErrorResponse(Exception exception,
	                                                  HttpStatus httpStatus,
	                                                  WebRequest request) {
		return buildErrorResponse(exception, exception.getMessage(), httpStatus, request);
	}

	private ResponseEntity<Object> buildErrorResponse(Exception exception,
	                                                  String message,
	                                                  HttpStatus httpStatus,
	                                                  WebRequest request) {
		ErrorResponse errorResponse = new ErrorResponse(httpStatus.value(), message);
		if (printStackTrace && isTraceOn(request)) {
			errorResponse.setStackTrace(ExceptionUtils.getStackTrace(exception));
		}
		return ResponseEntity.status(httpStatus).body(errorResponse);
	}

	private boolean isTraceOn(WebRequest request) {
		String[] value = request.getParameterValues(TRACE);
		return Objects.nonNull(value)
				&& value.length > 0
				&& value[0].contentEquals("true");
	}

	@Override
	public ResponseEntity<Object> handleExceptionInternal(
			Exception ex,
			Object body,
			HttpHeaders headers,
			HttpStatus status,
			WebRequest request) {

		return buildErrorResponse(ex, status, request);
	}
}
