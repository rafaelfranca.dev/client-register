package com.customer.register.controller;


import com.customer.register.model.User;
import com.customer.register.security.config.SecurityConfig;
import com.customer.register.security.domain.AuthenticationRequest;
import com.customer.register.security.domain.UserDetailsImpl;
import com.customer.register.service.impl.UserServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
@AutoConfigureMockMvc
@Slf4j
@ActiveProfiles(profiles = "test")
@WebAppConfiguration
class AuthControllerTest {

    @Autowired
    WebApplicationContext wac;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    AuthController controller;

    @MockBean
    UserServiceImpl service;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    SecurityConfig congig;

    @BeforeEach
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(wac)
                .apply(springSecurity())
                .build();
    }

    @Test
    void contextLoads() {
    }

    @Test
    void loginSuccess() throws Exception {

        UserDetailsImpl user = new UserDetailsImpl(User.builder()
                .username("admin")
                .password("123456")
                .role("ADMIN")
                .build());


        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(user.getUsername(),user.getPassword());

        Authentication authenticate = authenticationManager.authenticate(auth);

        when(service.loadUserByUsername(any())).thenReturn(user);

        AuthenticationRequest authenticationRequest = new AuthenticationRequest("admin","123456");

        ObjectMapper mapper = new ObjectMapper();

        String requestJson = mapper.writeValueAsString(authenticationRequest);

        this.mockMvc.perform(post("/api/auth/login")
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.user.username", Matchers.is(authenticationRequest.getUsername())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token", Matchers.notNullValue()));
    }

    @Test
    @ExceptionHandler(DisabledException.class)
    void loginDisabled() throws Exception {

        when(service.loadUserByUsername(any())).thenThrow(new DisabledException("USER_DISABLED"));

        AuthenticationRequest authenticationRequest = new AuthenticationRequest("admin","admin");

        ObjectMapper mapper = new ObjectMapper();

        String requestJson = mapper.writeValueAsString(authenticationRequest);

        this.mockMvc.perform(post("/api/auth/login")
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print());
    }

    @Test
    @ExceptionHandler(BadCredentialsException.class)
    void loginBadCredentials() throws Exception {

        when(service.loadUserByUsername(any())).thenThrow(new BadCredentialsException("BAD_CREDENTIAL"));

        AuthenticationRequest authenticationRequest = new AuthenticationRequest("unknown","unknown");

        ObjectMapper mapper = new ObjectMapper();

        String requestJson = mapper.writeValueAsString(authenticationRequest);

        this.mockMvc.perform(post("/api/auth/login")
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print());
    }
}
