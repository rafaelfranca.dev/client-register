package com.customer.register.controller;

import com.customer.register.model.Address;
import com.customer.register.model.Cellphone;
import com.customer.register.model.Client;
import com.customer.register.model.Email;
import com.customer.register.service.CellphoneService;
import com.customer.register.service.ClientService;
import com.customer.register.service.EmailService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.mockito.ArgumentMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
@Slf4j
@ActiveProfiles(profiles = "test")
class ClientControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ClientController controller;

    @MockBean
    ClientService service;

    @MockBean
    EmailService emailService;

    @MockBean
    CellphoneService cellphoneService;

    Client client;

    Address address;

    @BeforeEach
    void before(){

        address = new Address().builder()
                .address("Rua da Acácia")
                .city("Salvador")
                .district("Barra")
                .uf("BA")
                .zipCode("40215730")
                .build();

        client = new Client().builder()
                .name("Test")
                .cpf("86316734557")
                .address(address)
                .emails(Collections.singletonList(Email.builder().email("test@gmail.com").build()))
                .phones(Collections.singletonList(Cellphone.builder().type("residencial").number("71987505444").build()))
                .build();


    }

    @WithMockUser(username = "admin", password = "admin")
    @Test
    void create() throws Exception {

        ObjectMapper mapper = new ObjectMapper();

        String requestJson = mapper.writeValueAsString(client);

        client.setId(1L);

        Mockito.when(service.create(ArgumentMatchers.any())).thenReturn(client);

        this.mockMvc.perform(post("/api/client")
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.notNullValue()));
    }

    @WithMockUser(username = "admin", password = "admin")
    @Test
    void update() throws Exception {
        client.setId(1L);

        ObjectMapper mapper = new ObjectMapper();

        String requestJson = mapper.writeValueAsString(client);

        Mockito.when(service.update(ArgumentMatchers.any())).thenReturn(client);

        this.mockMvc.perform(put("/api/client")
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.notNullValue()));
    }

    @WithMockUser(username = "admin", password = "admin")
    @Test
    void listAll() throws Exception {

        List<Client> clientList = Collections.singletonList(client);

        Mockito.when(service.findAll()).thenReturn(clientList);

        this.mockMvc.perform(get("/api/client")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.notNullValue()));
    }

    @WithMockUser(username = "admin", password = "admin")
    @Test
    void findById() throws Exception {
        client.setId(1L);

        ObjectMapper mapper = new ObjectMapper();

        String requestJson = mapper.writeValueAsString(client);

        Mockito.when(service.find(ArgumentMatchers.any())).thenReturn(client);

        this.mockMvc.perform(get("/api/client/" + client.getId())
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.notNullValue()));
    }

    @WithMockUser(username = "admin", password = "admin")
    @Test
    void delete() throws Exception {
        Mockito.doNothing().when(service).delete(any());
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/api/client/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @WithMockUser(username = "admin", password = "admin")
    @Test
    void deleteCellphone() throws Exception {

        Mockito.doNothing().when(cellphoneService).delete(any());

        this.mockMvc.perform(MockMvcRequestBuilders.delete("/api/client/cellphone/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @WithMockUser(username = "admin", password = "admin")
    @Test
    void deleteEmail() throws Exception {

        Mockito.doNothing().when(emailService).delete(any());

        this.mockMvc.perform(MockMvcRequestBuilders.delete("/api/client/email/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}