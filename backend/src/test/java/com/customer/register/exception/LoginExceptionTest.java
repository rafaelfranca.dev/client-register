package com.customer.register.exception;

import com.customer.register.exceptions.LoginException;
import org.apache.commons.lang3.NotImplementedException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles(profiles = "test")
public class LoginExceptionTest {

    @Test
    void createInstace() {
        Assertions.assertThrows(LoginException.class, () -> {throw new LoginException("test");});
    }

    @Test
    void createInstaceW2Args() {
        Assertions.assertThrows(LoginException.class, () -> {throw new LoginException("test", new NullPointerException());});
    }
}
