package com.customer.register.persistence;

import com.customer.register.model.Address;
import com.customer.register.model.Cellphone;
import com.customer.register.model.Client;
import com.customer.register.model.Email;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@SpringBootTest
@ActiveProfiles(profiles = "test")
public class CellphoneDAOTest {

    @Autowired
    EmailDAO emailDAO;

    @Autowired
    AddressDAO addressDAO;

    @Autowired
    ClientDAO dao;

    @Autowired
    CellphoneDAO cellphoneDAO;

    Client defaultClient;

    Address address;

    Email email;

    Cellphone phone;

    @BeforeEach()
    void before() {

        defaultClient = new Client();
        defaultClient.setCpf("00000000000");
        defaultClient.setName("Luke Cake");

        address = new Address();
        address.setUf("BA");
        address.setAddress("Rua dos Orixás");
        address.setCity("Salvador");
        address.setDistrict("Federação");
        address.setZipCode("40000000");

        Long addressId = addressDAO.save(address);

        defaultClient.setAddress(Address.builder().id(addressId).build());

        Long clientId = dao.save(defaultClient);

        email = new Email();
        email.setEmail("fakeemail@gmil.com");
        email.setClient(Client.builder().id(clientId).build());
        emailDAO.save(email);
        defaultClient.setEmails(Collections.singletonList(email));

        phone = new Cellphone();
        phone.setType("celular");
        phone.setNumber("71987654321");
        phone.setClient(Client.builder().id(clientId).build());
        cellphoneDAO.save(phone);
        defaultClient.setPhones(Collections.singletonList(phone));
    }

    @Test
    @Transactional
    @Rollback(true)
    void findThrowsNotImplementedException() {
        assertThrows(NotImplementedException.class, () -> cellphoneDAO.find());
    }

    @Test
    @Transactional
    @Rollback
    void findByIdThrowsException() {
        assertThrows(Exception.class, () -> cellphoneDAO.find(null));
    }

    @Test
    @Transactional
    @Rollback(true)
    void findPhoneById() {
        List<Cellphone> phones = defaultClient.getPhones();
        log.info(phones.toString());
        for (Cellphone phone: phones) {
            Cellphone test = cellphoneDAO.find(phone.getId());
            log.info(test.toString());
            assertNotNull(test);
        }
    }


}
