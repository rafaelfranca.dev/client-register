package com.customer.register.persistence;

import com.customer.register.model.Address;
import com.customer.register.model.Cellphone;
import com.customer.register.model.Client;
import com.customer.register.model.Email;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.DisabledException;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Slf4j
@SpringBootTest
@ActiveProfiles(profiles = "test")
public class ClientDAOTest {

    @Autowired
    EmailDAO emailDAO;

    @Autowired
    AddressDAO addressDAO;

    @Autowired
    ClientDAO dao;

    @Autowired
    CellphoneDAO cellphoneDAO;

    Client defaultClient;

    Address address;

    Email email;

    Cellphone phone;

    @BeforeEach()
    void before() {

        defaultClient = new Client();
        defaultClient.setCpf("00000000000");
        defaultClient.setName("Luke Cake");

        address = new Address();
        address.setUf("BA");
        address.setAddress("Rua dos Orixás");
        address.setCity("Salvador");
        address.setDistrict("Federação");
        address.setZipCode("40000000");

        Long addressId = addressDAO.save(address);

        defaultClient.setAddress(Address.builder().id(addressId).build());

        Long clientId = dao.save(defaultClient);

        email = new Email();
        email.setEmail("fakeemail@gmil.com");
        email.setClient(Client.builder().id(clientId).build());
        emailDAO.save(email);
        defaultClient.setEmails(Collections.singletonList(email));

        phone = new Cellphone();
        phone.setType("celular");
        phone.setNumber("71987654321");
        phone.setClient(Client.builder().id(clientId).build());
        cellphoneDAO.save(phone);
        defaultClient.setPhones(Collections.singletonList(phone));
    }

    @Test
    @Transactional
    @Rollback(true)
    void testSave() {
        Client client = new Client();
        client.setCpf("00000000000");
        client.setName("Luke Cake");
        Address address = new Address();
        address.setUf("BA");
        address.setAddress("Rua dos Orixás");
        address.setCity("Salvador");
        address.setDistrict("Federação");
        address.setZipCode("40000000");
        Long addressId = addressDAO.save(address);
        client.setAddress(Address.builder().id(addressId).build());
        Long id = dao.save(client);
        Client test = dao.find(id);
        Assertions.assertEquals(test.getId(), id);
    }

    @Test
    @Transactional
    @Rollback(true)
    void testDelete() {
        Client client = dao.find(defaultClient.getId());
        if (client != null) {
            client.setAddress(addressDAO.find(client.getAddress().getId()));
            client.setPhones(cellphoneDAO.findByClient(client));
            client.setEmails(emailDAO.findByClient(client));

            if (client.getPhones() != null) {
                for (Cellphone phone : client.getPhones()) {
                    cellphoneDAO.delete(phone.getId());
                }
            }
            if (client.getEmails() != null) {
                for (Email email : client.getEmails()) {
                    emailDAO.delete(email.getId());
                }
            }
            dao.delete(defaultClient.getId());
            if (client.getAddress() != null && client.getAddress().getId() != null) {
                addressDAO.delete(client.getAddress().getId());
            }
        }
    }

    @Test
    @Transactional
    @Rollback(true)
    void testUpdate() {
        String oldName = defaultClient.getName();
        defaultClient.setName("Usuario Teste");
        dao.update(defaultClient);
        Client test = dao.find(defaultClient.getId());
        Assertions.assertNotEquals(test.getName(), oldName);
    }

    @Test
    @Transactional
    @Rollback(true)
    void testAddressUpdate() {
        defaultClient.setAddress(addressDAO.find(defaultClient.getAddress().getId()));
        String oldName = defaultClient.getAddress().getCity();
        defaultClient.getAddress().setCity("Lauro de freitas");
        addressDAO.update(defaultClient.getAddress());
        Address test = addressDAO.find(defaultClient.getAddress().getId());
        Assertions.assertNotEquals(test.getCity(), oldName);
    }

    @Test
    @Transactional
    @Rollback(true)
    void testFind() {
        List<Client> test = dao.find();
        Assertions.assertTrue(!test.isEmpty());
    }

    @Test
    @Transactional
    @Rollback(true)
    @ExceptionHandler({RuntimeException.class})
    void testDeleteError() {
        Assertions.assertThrows(RuntimeException.class, () -> dao.delete(100L));
    }


}

