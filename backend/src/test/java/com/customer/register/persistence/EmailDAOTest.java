package com.customer.register.persistence;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@SpringBootTest
@ActiveProfiles(profiles = "test")
public class EmailDAOTest {

    @Autowired
    EmailDAO emailDAO;

    @Test
    @Transactional
    @Rollback(true)
    void findThrowsNotImplementedException() {
        Assertions.assertThrows(NotImplementedException.class, () -> emailDAO.find());
    }

    @Test
    @Transactional
    @Rollback(true)
    void findWithIdThrowsNotImplementedException() {
        Assertions.assertThrows(NotImplementedException.class, () -> emailDAO.find(100L));
    }


}
