package com.customer.register.security.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

class AuthenticationRequestTest {

    AuthenticationRequest request;

    @BeforeEach
    public void setUp() {

        request = AuthenticationRequest.builder().username("admin").password("123456").build();
    }

    @Test
    void getUsername() {
        Assertions.assertEquals("admin", request.getUsername());
    }

    @Test
    void getPassword() {
        Assertions.assertEquals("123456", request.getPassword());
    }

    @Test
    void setUsername() {
        request.setUsername("user");
        Assertions.assertEquals("user", request.getUsername());
    }

    @Test
    void setPassword() {
        request.setPassword("testPassword");
        Assertions.assertEquals("testPassword", request.getPassword());
    }

    @Test
    void testToString() {
        Assertions.assertNotNull(request.toString());
    }
}