package com.customer.register.security.domain;

import com.customer.register.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AuthenticationResponseTest {

    @Test
    void builder() {
        AuthenticationResponse test =  AuthenticationResponse.builder().token("test").build();
        Assertions.assertNotNull(test);
        test.setUser(User.builder().password("admin").username("admin").build());
        Assertions.assertNotNull(test.getUser());
    }

    @Test
    void construct() {
        AuthenticationResponse test =  new AuthenticationResponse("token");
        Assertions.assertNotNull(test.toString());
        test = new AuthenticationResponse("Token", User.builder().build());
        Assertions.assertNotNull(test);
    }
}