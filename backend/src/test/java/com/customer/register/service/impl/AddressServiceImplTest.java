package com.customer.register.service.impl;

import com.customer.register.model.Address;
import com.customer.register.model.Cellphone;
import com.customer.register.model.Client;
import com.customer.register.persistence.AddressDAO;
import com.customer.register.persistence.CellphoneDAO;
import com.customer.register.service.AddressService;
import com.customer.register.service.CellphoneService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles(profiles = "test")
class AddressServiceImplTest {


    @Autowired
    AddressService service;

    @MockBean
    AddressDAO dao;

    Address address;

    @BeforeEach
    void before(){

        address = new Address().builder()
                .id(1L)
                .address("Rua da Acácia")
                .city("Salvador")
                .district("Barra")
                .uf("BA")
                .zipCode("40215730")
                .build();
    }

    @Test
    void create() {
        Mockito.when(dao.save(ArgumentMatchers.any())).thenReturn(1L);
        Address result = service.create(address);
        assertEquals(result.getId(), 1L);
    }

    @Test
    void update() {
        Mockito.doNothing().when (dao).update(ArgumentMatchers.any());
        Address result = service.update(address);
        assertEquals(result.getId(), 1L);
    }

    @Test
    void findAll() {
        NotImplementedException thrown = Assertions.assertThrows(NotImplementedException.class, () -> {
            service.findAll();
        });

        Assertions.assertNotNull(thrown);
    }

    @Test
    void find() {
        Mockito.when(dao.find(ArgumentMatchers.any())).thenReturn(address);
        Address result = service.find(1L);
        assertEquals(result.getId(), 1L);
    }

    @Test
    void delete() {
        Mockito.doNothing().when (dao).delete(ArgumentMatchers.any());
        service.delete(1L);
    }
}