package com.customer.register.service.impl;

import com.customer.register.model.Address;
import com.customer.register.model.Cellphone;
import com.customer.register.model.Client;
import com.customer.register.model.Email;
import com.customer.register.persistence.AddressDAO;
import com.customer.register.persistence.impl.ClientDAOImpl;
import com.customer.register.service.AddressService;
import com.customer.register.service.CellphoneService;
import com.customer.register.service.ClientService;
import com.customer.register.service.EmailService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles(profiles = "test")
class ClientServiceImplTest {

    @MockBean
    ClientDAOImpl dao;

    @Autowired
    ClientService service;

    @MockBean
    CellphoneService cellphoneService;

    @MockBean
    EmailService emailService;

    @MockBean
    AddressService addressService;

    Address address;

    Client client;

    Email email;

    Cellphone cellphone;

    @BeforeEach
    void setUp() {
        address = Address.builder()
                .id(1L)
                .address("Rua da Acácia")
                .city("Salvador")
                .district("Barra")
                .uf("BA")
                .zipCode("40215730")
                .build();

        email = Email.builder().email("test@gmail.com").build();

        cellphone = Cellphone.builder().type("residencial").number("71987505444").build();

        client = Client.builder()
                .name("Test")
                .cpf("86316734557")
                .address(address)
                .emails(Collections.singletonList(email))
                .phones(Collections.singletonList(cellphone))
                .build();
    }

    @Test
    void create() {

        email.setId(1L);
        cellphone.setId(1L);

        Mockito.when(dao.save(ArgumentMatchers.any())).thenReturn(1L);
        Mockito.when(cellphoneService.create(ArgumentMatchers.any())).thenReturn(cellphone);
        Mockito.when(emailService.create(ArgumentMatchers.any())).thenReturn(email);

        Client savedClient = service.create(client);

        Assertions.assertNotNull(savedClient);
        Assertions.assertEquals(savedClient.getId(),1L);
        Assertions.assertNotNull(savedClient.getPhones());
        Assertions.assertNotNull(savedClient.getEmails());
    }

    @Test
    void update() {
        Mockito.doNothing().when(dao).update(ArgumentMatchers.any());

        service.update(client);
    }

    @Test
    void find() {
        Mockito.when(dao.find(ArgumentMatchers.any())).thenReturn(client);
        Mockito.when(cellphoneService.create(ArgumentMatchers.any())).thenReturn(cellphone);
        Mockito.when(emailService.create(ArgumentMatchers.any())).thenReturn(email);

        service.find(1L);
    }

    @Test
    void findAll() {
        Mockito.when(dao.find()).thenReturn(Collections.singletonList(client));
        Mockito.when(cellphoneService.create(ArgumentMatchers.any())).thenReturn(cellphone);
        Mockito.when(emailService.create(ArgumentMatchers.any())).thenReturn(email);

        service.findAll();
    }

    @Test
    void delete() {
        email.setId(1L);
        cellphone.setId(1L);
        Mockito.when(dao.find(ArgumentMatchers.any())).thenReturn(client);

        Mockito.when(addressService.find(ArgumentMatchers.any())).thenReturn(address);
        Mockito.when(cellphoneService.findByClient(ArgumentMatchers.any())).thenReturn(Collections.singletonList(cellphone));
        Mockito.when(emailService.findByClient(ArgumentMatchers.any())).thenReturn(Collections.singletonList(email));

        Mockito.doNothing().when(addressService).delete(ArgumentMatchers.any());
        Mockito.doNothing().when(cellphoneService).delete(ArgumentMatchers.any());
        Mockito.doNothing().when(emailService).delete(ArgumentMatchers.any());

        service.delete(1L);
    }
}