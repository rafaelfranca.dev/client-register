package com.customer.register.service.impl;

import com.customer.register.model.Address;
import com.customer.register.model.Cellphone;
import com.customer.register.model.Client;
import com.customer.register.model.Email;
import com.customer.register.persistence.EmailDAO;
import com.customer.register.service.EmailService;
import org.apache.commons.lang3.NotImplementedException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.omg.CORBA.portable.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles(profiles = "test")
class EmailServiceImplTest {

    @Autowired
    EmailService service;

    @MockBean
    EmailDAO dao;

    Email email;

    @BeforeEach
    void before(){

        email = new Email().builder()
                .email("test@gmail.com")
                .client(Client.builder().id(1L).build())
                .build();
    }

    @Test
    void create() {
        Mockito.when(dao.save(ArgumentMatchers.any())).thenReturn(1L);
        Email result = service.create(email);
        assertEquals(result.getClient().getId(), 1L);
    }

    @Test
    void update() {

        NotImplementedException thrown = Assertions.assertThrows(NotImplementedException.class, () -> {
            service.update(null);
        });

        Assertions.assertNotNull(thrown);
    }

    @Test
    void find() {
        NotImplementedException thrown = Assertions.assertThrows(NotImplementedException.class, () -> {
            service.find(null);
        });

        Assertions.assertNotNull(thrown);
    }

    @Test
    void findAll() {
        NotImplementedException thrown = Assertions.assertThrows(NotImplementedException.class, () -> {
            service.findAll();
        });

        Assertions.assertNotNull(thrown);
    }

    @Test
    void delete() {
        Mockito.doNothing().when(dao).delete(ArgumentMatchers.any());
        service.delete(1L);
    }

    @Test
    void findByClient() {
        Mockito.when(dao.findByClient(ArgumentMatchers.any())).thenReturn(Collections.singletonList(email));
        List<Email> result = service.findByClient(Client.builder().id(1L).build());
        Assertions.assertNotNull(result);
    }
}