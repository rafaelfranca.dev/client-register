package com.customer.register.service.impl;

import com.customer.register.model.User;
import com.customer.register.security.domain.UserDetailsImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles(profiles = "test")
class JwtTokenServiceTest {


    @Autowired
    JwtTokenService service;

    @Test
    void setSecretKey() {
    }

    @Test
    void setJwtExpirationInMs() {
    }

    @Test
    void init() {
    }

    @Test
    void generateToken() {
        UserDetailsImpl user = new UserDetailsImpl(User.builder()
                .username("admin")
                .password("admin")
                .role("ADMIN")
                .build());

        String token = service.generateToken(user);
        System.out.println(token);
    }

    @Test
    void validateToken() {
        UserDetailsImpl user = new UserDetailsImpl(User.builder()
                .username("admin")
                .password("admin")
                .role("ADMIN")
                .build());

        String token = service.generateToken(user);

        service.validateToken(token);

    }

    @Test
    void getUsernameFromToken() {
        UserDetailsImpl user = new UserDetailsImpl(User.builder()
                .username("admin")
                .password("admin")
                .role("ADMIN")
                .build());

        String token = service.generateToken(user);

        service.getUsernameFromToken(token);
    }
}