package com.customer.register.service.impl;

import com.customer.register.service.LocationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles(profiles = "test")
class LocationServiceImplTest {

    @Autowired
    LocationService service;

    @Test
    void findCep() {
        String response = service.findCep("40215730");
        Assertions.assertNotNull(response);
    }
}