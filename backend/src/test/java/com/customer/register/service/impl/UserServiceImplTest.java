package com.customer.register.service.impl;

import com.customer.register.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles(profiles = "test")
class UserServiceImplTest {

    @Autowired
    UserService service;

    @Test
    void loadUserByUsernameAdmin() {
        UserDetails user =  service.loadUserByUsername("admin");
        assertEquals(user.isEnabled(),true);
        assertEquals(user.getUsername(),"admin");
    }

    @Test
    void loadUserByUsernameUser() {
        UserDetails user =  service.loadUserByUsername("user");
        assertEquals(user.isEnabled(),true);
        assertEquals(user.getUsername(),"user");
    }
}