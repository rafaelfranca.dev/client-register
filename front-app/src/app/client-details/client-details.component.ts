import { TokenStorageService } from './../services/token-storage.service';
import { ToastrService } from 'ngx-toastr';
import { Email } from './../model/email';
import { Phone } from './../model/phone';
import { LocationService } from './../services/location.service';
import { Client } from './../model/client';
import { ClientService } from './../services/client.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Address } from '../model/address';

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.css']
})
export class ClientDetailsComponent implements OnInit {

  title = "Adicionar Cliente";

  id: any;

  phone = new Phone();

  userEmail = new Email();

  phoneList: Phone[] = [];

  emailList: Email[] = [];

  client: Client = new Client();

  isUpdate = false;

  loading = false;

  isAdmin = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private clientService: ClientService,
    private locationService: LocationService,
    private toastr: ToastrService,
    private tokenStorageService: TokenStorageService) {
    this.client.address = new Address();
  }

  ngOnInit(): void {

    const user = this.tokenStorageService.getUser();

    if (user.role === 'ADMIN') {
      this.isAdmin = true;
    }

    this.id = this.route.snapshot.paramMap.get('id');

    if (this.id) {
      this.isUpdate = true;
      this.reloadData();
      this.title = "Editar Cliente"
    }

  }

  onSubmit(): void {

    this.client.emails = this.emailList;

    this.client.phones = this.phoneList;

    this.loading = true;
    if (this.isUpdate) {
      this.clientService.updateClient(this.client).subscribe(
        data => {
          this.loading = false;
          this.toastr.success('Salvo com sucesso!');
          this.router.navigate(['/client/' + data.id]);
        },
        err => {
          this.loading = false;
          this.toastr.error('Erro ao salvar!');
          console.error(err);
        }
      );
    } else {
      this.clientService.saveClient(this.client).subscribe(
        data => {
          this.loading = false;
          this.toastr.success('Salvo com sucesso!');
          this.router.navigate(['/client/' + data.id]);
        },
        err => {
          this.loading = false;
          this.toastr.error('Erro ao salvar!');
          console.error(err);
        }
      );
    }
  }

  timeout: any = null;

  public onKeySearch(event: any) {
    clearTimeout(this.timeout);
    const $this = this;
    this.timeout = setTimeout(function () {
      if (event.keyCode !== 13) {
        $this.consultCep(event.target.value);
      }
    }, 800);
  }

  private consultCep(cep: string) {

    if (cep && cep.length >= 9) {
      cep = this.removeMask(cep);
      console.log('cep:', cep);

      this.locationService.getCep(cep).subscribe(data => {
        this.client.address = new Address();
        this.client.address.zipCode = cep;
        this.client.address.city = data.localidade;
        this.client.address.district = data.bairro;
        this.client.address.uf = data.uf;
        this.client.address.address = data.logradouro;
      }, (error) => {
        console.error(error);
      });
    }
  }

  reloadData() {
    this.clientService.findById(this.id).subscribe((res: Client) => {
      this.client = res;
      this.phoneList = res.phones;
      this.emailList = res.emails;;
    });
  }

  removeMask(data: string) {
    data = data.split('.').join("");
    data = data.split('-').join("");
    return data;
  }

  addPhone() {
    if (this.isUpdate) {
      this.clientService.saveCellphone(this.client.id, this.phone).subscribe(() => {
        this.toastr.success('Telefone Adicionado!');
        this.reloadData();
      });
    } else {
      this.phoneList.push(this.phone);
    }
    this.phone = new Phone();
  }

  addMail() {
    if (this.isUpdate) {
      this.clientService.saveEmail(this.client.id, this.userEmail).subscribe(() => {
        this.toastr.success('Email Adicionado!');
        this.reloadData();
      });
    } else {
      this.emailList.push(this.userEmail);
    }
    this.userEmail = new Email();
  }

  deleteCellphone(id: any) {
    if (this.isUpdate) {
      if (this.phoneList.length < 2) {
        this.toastr.warning('não permitido remover todos telefones!');
        return;
      }
      this.clientService.deleteCellphone(id).subscribe(() => {
        this.toastr.success('Telefone Apagado!');
        this.reloadData();
      });
    } else {
      this.phoneList = this.phoneList.filter((element) => {
        return element.id !== id;
      });
    }
  }

  deleteEmail(id: any) {
    if (this.isUpdate) {
      if (this.emailList.length < 2) {
        this.toastr.warning('não permitido remover todos emails!',);
        return;
      }
      this.clientService.deleteEmail(id).subscribe(() => {
        this.toastr.success('Email Apagado!');
        this.reloadData();
      });
    } else {
      this.emailList = this.emailList.filter((element) => {
        return element.id !== id;
      });
    }
  }
}
