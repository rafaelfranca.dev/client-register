import { TokenStorageService } from './../services/token-storage.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Client } from './../model/client';
import { ClientService } from './../services/client.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  content?: Client[];

  isAdmin = false;

  constructor(
    private clientService: ClientService,
    private toastr: ToastrService,
    private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {

    const user = this.tokenStorageService.getUser();

    if (user.role === 'ADMIN') {
      this.isAdmin = true;
    }

    this.reloadClients();
  }

  delete(id: any) {
    if (confirm("Deletar Cliente? ")) {
      this.clientService.delete(id).subscribe(() => {
        this.reloadClients();
        this.toastr.success("Apagado!");
      }, err => {
        this.toastr.error("Erro ao apagar:", err);
        console.error(err);
      });
    }
  }

  reloadClients() {
    this.clientService.findAll().subscribe(
      data => {
        this.content = data;
      },
      err => {
        this.content = JSON.parse(err.error).message;
      }
    );
  }


}
