export class Address {
  id?: number;
  zipCode?: string;
  city!: string;
  uf?: string;
  district?: string;
  address?: string;
  complement?: string;

  constructor() {
  }
}
