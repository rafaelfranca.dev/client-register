import { Email } from './email';
import { Phone } from './phone';
import { Address } from './address';
export class Client {
  id!: number;
  name!: string;
  cpf!: string;
  address!: Address;
  phones!: Phone[];
  emails!: Email[];
}
