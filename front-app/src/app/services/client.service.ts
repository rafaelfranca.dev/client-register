import { Phone } from './../model/phone';
import { Email } from './../model/email';
import { Client } from './../model/client';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

const API_URL = 'http://localhost:8080/api/client/';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) { }

  findAll(): Observable<Client[]> {
    return this.http.get<Client[]>(API_URL);
  }

  findById(id: number): Observable<any> {
    return this.http.get<Client[]>(API_URL + id);
  }

  saveClient(client: Client): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
    return this.http.post<Client>(API_URL, client, {
      headers: headers
    });
  }

  updateClient(client: Client): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
    return this.http.put<Client>(API_URL, client, {
      headers: headers
    });
  }

  delete(id: number): Observable<any> {
    return this.http.delete(API_URL + id);
  }

  saveCellphone(id: number, phone: Phone): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
    return this.http.post(API_URL + id + '/cellphone', phone, {
      headers: headers
    });
  }

  saveEmail(id: number, email: Email): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
    return this.http.post(API_URL + id + '/email', email, {
      headers: headers
    });
  }

  deleteCellphone(id: number): Observable<any> {
    return this.http.delete(API_URL + 'cellphone/' + id);
  }

  deleteEmail(id: number): Observable<any> {
    return this.http.delete(API_URL + 'email/' + id);
  }
}
