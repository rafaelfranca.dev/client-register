import { CepRepresentation } from './../model/cep-response.representation';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

const API_URL = 'http://localhost:8080/api/location/';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private http: HttpClient) { }

  getCep(cep: string) {
    return this.http.get<CepRepresentation>(
      API_URL + `cep/` + cep
    );
  }
}
